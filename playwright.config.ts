import type { PlaywrightTestConfig } from "@playwright/test";
const { devices } = require('playwright');

/**
 * See https://playwright.dev/docs/test-configuration.
 */
const config: PlaywrightTestConfig = {
  testDir: "./tests",
  timeout: 30 * 1000,
  expect: {
    timeout: 5000,
  },
  fullyParallel: true,
  retries: 1,
  workers: 2,
  reporter: [["line"], ["allure-playwright"]],
  use: {
    headless: false,
    actionTimeout: 0,
    trace: "on-first-retry",
    video: 'on'
  },
  projects: [
    {
      name: 'Chrome',
      use: {
        channel: 'chrome',
      },
    },
    {
      name: 'Mobile',
      use: {
        browserName: 'chromium',
        ...devices['Pixel 4'],
      },
    },
  ],
};

export default config;
