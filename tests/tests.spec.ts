import { test, expect, Page } from "@playwright/test";

test.describe("Tests", () => {
  test("firstTests", async ({ page }) => {
    await test.step("Донор переходит на сайт", async () => {
      await page.goto("https://data.fundraiseup.com/qa-test-7R58U3/");
    });

    await test.step("Кликает на кнопку “Give now”", async () => {
      await page
        .frameLocator('//*[@title="Donate Button"]')
        .locator('//*[@qa="fun-element"]')
        .click();
    });

    const donationWidgetFrame = page.frameLocator(
      '//*[@title="Donation Widget"]'
    );

    await test.step("Выбирает “Monthly” пожертвование", async () => {
      await donationWidgetFrame.locator('//*[@data-qa="give-monthly"]').click();
    });

    await test.step("Вводит сумму 100 USD", async () => {
      await donationWidgetFrame
        .locator('//*[@data-qa="currency-selector"]')
        .selectOption("USD");
      await donationWidgetFrame.locator('//*[@data-qa="amount"]').fill("100");
    });

    await test.step("Нажимает “Donate monthly”", async () => {
      await donationWidgetFrame
        .locator('//*[@data-qa="donate-button"]')
        .click();
    });

    await test.step(
      "Убирает чек-бокс покрытия комиссии “Cover transaction costs”",
      async () => {
        await donationWidgetFrame
          .locator('//div[@data-qa="cover-fee-checkbox"]')
          .click();
      }
    );

    await test.step(
      "Выбирает оплату кредитной картой “Credit card”",
      async () => {
        await donationWidgetFrame.locator('//*[@data-qa="cc-button"]').click();
      }
    );

    await test.step("Вводит карточные данные для оплаты", async () => {
      const cardFrame = donationWidgetFrame.frameLocator(
        '//*[@title="Secure card number input frame"]'
      );
      await cardFrame
        .locator('//input[@name="cardnumber"]')
        .fill("4242 4242 4242 4242");

      const expireDateFrame = donationWidgetFrame.frameLocator(
        '//*[@title="Secure expiration date input frame"]'
      );
      await expireDateFrame.locator('//input[@name="exp-date"]').fill("0424");

      const cvcCardFrame = donationWidgetFrame.frameLocator(
        '//*[@title="Secure CVC input frame"]'
      );
      await cvcCardFrame.locator('//input[@name="cvc"]').fill("000");
    });

    await test.step("Нажимает “Continue”", async () => {
      await donationWidgetFrame
        .locator('//button[@qa="pay-button" and @id="credit-card-button"]')
        .click();
    });

    await test.step("Вводит “First name”", async () => {
      await donationWidgetFrame
        .locator('//*[@data-qa="personal-first-name"]')
        .fill("Иван");
    });

    await test.step("Вводит “Last name”", async () => {
      await donationWidgetFrame
        .locator('//*[@data-qa="personal-last-name"]')
        .fill("Иванов");
    });

    await test.step("Вводит “E-mail”", async () => {
      await donationWidgetFrame
        .locator('//*[@data-qa="personal-email"]')
        .fill("swetafeox@gmail.com");
    });

    await test.step("Нажимает “Donate”", async () => {
      await donationWidgetFrame
        .locator(
          "//button[contains(@qa,'pay-button') and contains(@id, 'privacy-pay-button')]"
        )
        .click();
      await expect(
        donationWidgetFrame.locator('//*[@qa="error-message"]')
      ).toHaveText("Your card was declined.");
      await expect(
        donationWidgetFrame.locator('//*[@data-qa="card-error-message"]')
      ).toHaveText(
        "This could be due to any of several reasons: incorrect security code, insufficient funds, card limit, card disabled, etc."
      );
    });
  });

  test("secondTests", async ({ page }) => {
    await test.step("Донор переходит на сайт", async () => {
      await page.goto("https://data.fundraiseup.com/qa-test-7R58U3/");
    });

    await test.step("Кликает на кнопку “Give now”", async () => {
      await page
        .frameLocator('//*[@title="Donate Button"]')
        .locator('//*[@qa="fun-element"]')
        .click();
    });

    const donationWidgetFrame = page.frameLocator(
      '//*[@title="Donation Widget"]'
    );

    await test.step("Выбирает “Monthly” пожертвование", async () => {
      await donationWidgetFrame.locator('//*[@data-qa="give-monthly"]').click();
    });

    await test.step("Вводит сумму 100 USD", async () => {
      await donationWidgetFrame
        .locator('//*[@data-qa="currency-selector"]')
        .selectOption("USD");
      await donationWidgetFrame.locator('//*[@data-qa="amount"]').fill("100");
    });

    await test.step("Нажимает “Donate monthly”", async () => {
      await donationWidgetFrame
        .locator('//*[@data-qa="donate-button"]')
        .click();
    });

    await test.step(
      "Убирает чек-бокс покрытия комиссии “Cover transaction costs”",
      async () => {
        if (donationWidgetFrame.locator('//div[@aria-checked="true"]')) {
          await donationWidgetFrame
            .locator('//div[@data-qa="cover-fee-checkbox"]')
            .click();
        }
      }
    );

    await test.step(
      "Выбирает оплату кредитной картой “Credit card”",
      async () => {
        await donationWidgetFrame.locator('//*[@data-qa="cc-button"]').click();
      }
    );

    await test.step("Вводит карточные данные для оплаты", async () => {
      const cardFrame = donationWidgetFrame.frameLocator(
        '//*[@title="Secure card number input frame"]'
      );
      await cardFrame
        .locator('//input[@name="cardnumber"]')
        .fill("4242 4242 4242 4242");

      const expireDateFrame = donationWidgetFrame.frameLocator(
        '//*[@title="Secure expiration date input frame"]'
      );
      await expireDateFrame.locator('//input[@name="exp-date"]').fill("0424");

      const cvcCardFrame = donationWidgetFrame.frameLocator(
        '//*[@title="Secure CVC input frame"]'
      );
      await cvcCardFrame.locator('//input[@name="cvc"]').fill("000");
    });

    await test.step("Нажимает “Continue”", async () => {
      await donationWidgetFrame
        .locator('//button[@qa="pay-button" and @id="credit-card-button"]')
        .click();
    });

    await test.step("Вводит “First name”", async () => {
      await donationWidgetFrame
        .locator('//*[@data-qa="personal-first-name"]')
        .fill("Иван");
    });

    await test.step("Вводит “Last name”", async () => {
      await donationWidgetFrame
        .locator('//*[@data-qa="personal-last-name"]')
        .fill("Иванов");
    });

    await test.step("Вводит “E-mail”", async () => {
      await donationWidgetFrame
        .locator('//*[@data-qa="personal-email"]')
        .fill("swetafeox@gmail.com");
    });

    await test.step("Нажимает “Donate”", async () => {
      await donationWidgetFrame
        .locator(
          "//button[contains(@qa,'pay-button') and contains(@id, 'privacy-pay-button')]"
        )
        .click();
      await expect(
        donationWidgetFrame.locator('//*[@qa="error-message"]')
      ).toHaveText("Your card was declined.");
      await expect(
        donationWidgetFrame.locator('//*[@data-qa="card-error-message"]')
      ).toHaveText(
        "This could be due to any of several reasons: incorrect security code, insufficient funds, card limit, card disabled, etc."
      );
    });
  });
});
