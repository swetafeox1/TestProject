# TestProject

## Установить пакеты:

```npm i```

## Установить allure в зависимости от вашей операционной системы:

https://docs.qameta.io/allure/#_installing_a_commandline

## Запуск тестов:

```npx playwright test```

## Запуск тестов с режимом дебага:

```npx playwright test --debug```

## Запуск тестов только в Chrome:

```npx playwright test --project=Chrome```

## Запуск тестов только в Mobile:

```npx playwright test --project=Mobile```

## Отчет в аллюре (выполнять после прогона тестов):

```allure serve```

## Чтобы запустить тесты параллельно (по дефолту в конфиге уже стоит fullyParallel: true и  workers: 2):

```npx playwright test --workers=2```

## Чтобы запустить тесты последовательно, последовательность запуска указана в конфиге в project (сначала запуститься Desktop, потом Mobile):

```npx playwright test --workers=1```
